#! /usr/local/opt/ruby/bin/ruby

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'optparse'
require 'json'

options = {}
OptionParser.new do |opt|
  opt.on('--list') { |_| options[:list] = true }
  opt.on('--host') { |_| options[:host] = true }
end.parse!

if options[:list]
  puts IO.read('./a.json')
else
  a = {_meta: {hostvar: {}}}.to_json
  puts a
end
